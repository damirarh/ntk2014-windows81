﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using Windows.UI.Xaml.Shapes;

namespace RenderTargetBitmapDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Random _random = new Random();
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnAdd(object sender, RoutedEventArgs e)
        {
            var element = new Ellipse();
            element.Width = _random.NextDouble() * 100;
            element.Height = _random.NextDouble() * 100;
            element.Opacity = _random.NextDouble();
            element.Fill = new SolidColorBrush { Color = Colors.Blue };
            element.SetValue(Canvas.LeftProperty, _random.NextDouble() * RootElement.ActualWidth);
            element.SetValue(Canvas.TopProperty, _random.NextDouble() * RootElement.ActualHeight);
            RootElement.Children.Add(element);
        }

        private async void OnSave(object sender, RoutedEventArgs e)
        {
            var renderTargetBitmap = new RenderTargetBitmap();
            await renderTargetBitmap.RenderAsync(RootElement);

            var picker = new FileSavePicker();
            picker.FileTypeChoices.Add("PNG Image", new List<string>() { ".png" });
            var file = await picker.PickSaveFileAsync();

            using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, 
                    (uint)renderTargetBitmap.PixelWidth, (uint)renderTargetBitmap.PixelHeight, 96, 96, 
                    (await renderTargetBitmap.GetPixelsAsync()).ToArray());
        
                await encoder.FlushAsync();
            }
        }
    }
}
