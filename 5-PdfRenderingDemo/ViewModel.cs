﻿using System;
using System.Windows.Input;
using Windows.ApplicationModel;
using Windows.Data.Pdf;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Microsoft.Practices.Prism.StoreApps;

namespace PdfRenderingDemo
{
    public class ViewModel : BindableBase
    {
        private PdfDocument _pdfDoc;
        private ImageSource _pageImageSource;
        private int _pageNo;
        public ICommand PrevCommand { get; set; }
        public ICommand NextCommand { get; set; }

        public ImageSource PageImageSource
        {
            get { return _pageImageSource; }
            set { SetProperty(ref _pageImageSource, value); }
        }

        public int PageNo
        {
            get { return _pageNo; }
            set { SetProperty(ref _pageNo, value); }
        }

        public ViewModel(string filename)
        {
            Init(filename);

            NextCommand = new DelegateCommand(OnNext);
            PrevCommand = new DelegateCommand(OnPrev);
        }

        private void OnNext()
        {
            if (_pdfDoc.PageCount > PageNo)
            {
                PageNo++;
                DisplayPage();
            }
        }

        private void OnPrev()
        {
            if (1 < PageNo)
            {
                PageNo--;
                DisplayPage();
            }
        }

        private async void DisplayPage()
        {
            var page = _pdfDoc.GetPage((uint)PageNo - 1);
            var tempFile = await ApplicationData.Current.TemporaryFolder.CreateFileAsync("page.jpg", CreationCollisionOption.GenerateUniqueName);
            using (var stream = await tempFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                await page.RenderToStreamAsync(stream);
                await stream.FlushAsync();
                stream.Seek(0);
                var bitmap = new BitmapImage();
                await bitmap.SetSourceAsync(stream);
                PageImageSource = bitmap;
            }
        }

        private async void Init(string filename)
        {
            var file = await Package.Current.InstalledLocation.GetFileAsync(filename);
            _pdfDoc = await PdfDocument.LoadFromFileAsync(file);
            PageNo = 1;
            DisplayPage();
        }
    }
}