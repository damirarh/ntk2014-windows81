﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using Windows.Web.Http;

namespace WebViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnNavigate(object sender, RoutedEventArgs e)
        {
            var values = new Dictionary<string, string>
            {
                {"Key", "Value"}
            };
            var content = new HttpFormUrlEncodedContent(values);
            var request = new HttpRequestMessage(
                HttpMethod.Post,
                new Uri("http://www.htmlcodetutorial.com/cgi-bin/mycgi.pl"))
            {
                Content = content
            };
            WebView.NavigateWithHttpRequestMessage(request);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += OnDataRequested;
        }

        private async void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var dataRequest = args.Request;
            var deferral = dataRequest.GetDeferral();
            var dataPackage = await WebView.CaptureSelectedContentToDataPackageAsync();
            if (dataPackage != null)
            {
                dataRequest.Data = dataPackage;
                dataRequest.Data.Properties.Title = "NTK 2014";
                dataRequest.Data.Properties.Description = "WebView sharing";
            }
            deferral.Complete();
        }
    }
}
