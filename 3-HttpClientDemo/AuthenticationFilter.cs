﻿using System;
using Windows.Foundation;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace HttpClientDemo
{
    public class AuthenticationFilter : IHttpFilter
    {
        private readonly IHttpFilter _innerFilter;

        public string Token { get; set; }

        public AuthenticationFilter(IHttpFilter innerFilter)
        {
            _innerFilter = innerFilter;
        }

        public void Dispose()
        { }

        public IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> SendRequestAsync(HttpRequestMessage request)
        {
            if (!String.IsNullOrEmpty(Token))
            {
                request.Headers.Add("X-Token", Token);
            }
            return _innerFilter.SendRequestAsync(request);
        }
    }
}