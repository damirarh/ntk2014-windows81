﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace HttpClientDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnPost(object sender, RoutedEventArgs e)
        {
            var baseFilter = new HttpBaseProtocolFilter();
            baseFilter.CookieManager.SetCookie(new HttpCookie("Test", ".cylog.org", "/") {Value = "Value"});
            var authenticationFilter = new AuthenticationFilter(baseFilter);
            authenticationFilter.Token = "AuthTokenValue";
            var httpClient = new HttpClient(authenticationFilter);
            httpClient.DefaultRequestHeaders.IfModifiedSince = new DateTime(2013, 1, 1);
            var page = await httpClient.GetStringAsync(new Uri("http://www.cylog.org/headers/"));
            WebView.NavigateToString(page);
        }
    }
}
