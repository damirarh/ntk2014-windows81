﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;


namespace CodedUITest
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class CodedUiTest
    {
        [TestMethod]
        public void InitTestMethod()
        {
            XamlWindow.Launch("7d0a28df-2032-4180-8fa8-b1831a842cf8_ecet6zh215f6e!App");
            Assert.AreEqual("None", UIMap.UISampleAppWindow.UILabelText.DisplayText);
        }

        [TestMethod]
        public void FirstTestMethod()
        {
            XamlWindow.Launch("7d0a28df-2032-4180-8fa8-b1831a842cf8_ecet6zh215f6e!App");
            Gesture.Tap(UIMap.UISampleAppWindow.UIFirstButton);
            Assert.AreEqual("First", UIMap.UISampleAppWindow.UILabelText.DisplayText);
        }

        [TestMethod]
        public void SecondTestMethod()
        {
            XamlWindow.Launch("7d0a28df-2032-4180-8fa8-b1831a842cf8_ecet6zh215f6e!App");
            Gesture.Tap(UIMap.UISampleAppWindow.UISecondButton);
            Assert.AreEqual("Second", UIMap.UISampleAppWindow.UILabelText.DisplayText);
        }

        [TestMethod]
        public void ThirdTestMethod()
        {
            XamlWindow.Launch("7d0a28df-2032-4180-8fa8-b1831a842cf8_ecet6zh215f6e!App");
            Gesture.Tap(UIMap.UISampleAppWindow.UIThirdButton);
            Assert.AreEqual("Third", UIMap.UISampleAppWindow.UILabelText.DisplayText);
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
