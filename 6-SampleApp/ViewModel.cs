﻿using System.Windows.Input;
using Microsoft.Practices.Prism.StoreApps;

namespace SampleApp
{
    public class ViewModel : BindableBase
    {
        private string _label;

        public string Label
        {
            get { return _label; }
            set { SetProperty(ref _label, value); }
        }

        public ICommand ClickCommand { get; set; }

        public ViewModel()
        {
            Label = "None";
            ClickCommand = new DelegateCommand<string>(arg => Label = arg);
        }
    }
}