﻿using System;
using System.Collections.ObjectModel;
using Windows.Devices.Geolocation;
using Windows.Devices.Geolocation.Geofencing;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GeofencingDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<string> GeofenceEvents { get; set; }

        public MainPage()
        {
            this.InitializeComponent();

            GeofenceEvents = new ObservableCollection<string>();
            DataContext = this;

            GeofenceMonitor.Current.Geofences.Clear();

            CreateGeofence(46.119944, 14.815333, 100);
            GeofenceMonitor.Current.GeofenceStateChanged += OnGeofenceStateChanged;
        }

        private async void OnGeofenceStateChanged(GeofenceMonitor sender, object args)
        {
            var reports = sender.ReadReports();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                foreach (var report in reports)
                {
                    var state = report.NewState;
                    var geofence = report.Geofence;

                    GeofenceEvents.Add(String.Format("{0}: {1}", state, geofence.Id));
                }
            });
        }

        private void CreateGeofence(double latitude, double longitude, double radius)
        {
            var id = string.Format("Position: {0}, {1}", latitude, longitude);

            var position = new BasicGeoposition
            {
                Latitude = latitude,
                Longitude = longitude
            };

            var geocircle = new Geocircle(position, radius);
            var states = MonitoredGeofenceStates.Entered | MonitoredGeofenceStates.Exited;
            var dwellTime = TimeSpan.FromSeconds(5);

            var geofence = new Geofence(id, geocircle, states, false, dwellTime);
            GeofenceMonitor.Current.Geofences.Add(geofence);
        }
    }
}
